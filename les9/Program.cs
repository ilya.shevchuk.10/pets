﻿using System;

namespace les9
{
    class Program
    {
        static void Main(string[] args)
        {
            string a = "Asdsdsdss";
            string l = "s";
            int t=StrCount3(a,l);
            Console.WriteLine(t);
            int[] array = Divisors1(10);
            string st = "abc";
            int[] arr = { 0,0,0 };
            string rez = LastSurvivor(st, arr);


        }

        public static int StrCount(string str, string letter)
        {

            int counter = 0;
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i].ToString() == letter)
                {
                    counter++;
                }
            }
            return counter;
        }

        public static int StrCount2(string str, string letter)
        {
            int strLeng = str.Length;
            str = str.Replace(letter, "");
            return strLeng-str.Length;
        }

        public static int StrCount3(string str, string letter)
        {
            int counter = 0;
            foreach (char item in str)
            {
                if (item.ToString() == letter)
                {
                    counter++;
                }

            }
            return counter;
        }
        public static int[] Divisors(int n)
        {
            int[] arr = new int[0];
            int c = 0;
            
            for (int i = 2; i < n; i++)
            { 
                if (n % i == 0)
                {
                    c++;
                    Array.Resize(ref arr, arr.Length + 1);
                    arr[c-1] = i;
                }
                
            }
            return arr;
        }
        public static int[] Divisors1(int n)
        {
            int[] arr = new int[0];
            int c = 0;
            int i = 2;
            while (i<n)
            {
                if (n % i == 0)
                {
                    c++;
                    Array.Resize(ref arr, arr.Length + 1);
                    arr[c - 1] = i;
                }
                i++;
            }
            return arr;
        }
        public static string LastSurvivor(string letters, int[] coords)
        {
            foreach (var item in coords)
            {
                letters = letters.Remove(item,1);
            }
            return letters;
        }
    }
}
