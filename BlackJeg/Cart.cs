﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlackJeg
{
    public class Cart
    {
        public int value;
        public string name;
        public Suit suit;
        public Cart(int value, string name, Suit suit)
        {
            this.value = value;
            this.name = name;
            this.suit = suit;
        }

    }
    
    
    public enum Suit  
    {
        Clubs, 
        Diamonds, 
        Hearts,
        Spades
    }
}
