﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlackJeg
{
    public class Player
    {
        public string name;
        public Cart[] carts;
        public bool isInGame;
        public Player(string name, Cart[] carts)
        {
            this.name = name;
            this.carts = carts;
            this.isInGame = true;
        }
        public Player(string name)
        {
            this.name = name;
            carts = new Cart[0];
            isInGame = true;
        }
        public int Res()
        {
            int res = 0;
            for (int i = 0; i < carts.Length; i++)
            {
                res = carts[i].value + res;
            }
            return res;
        }
        public void SetCart(Cart cart)
        {
            Cart[] temp = new Cart[carts.Length + 1];
            for (int i = 0; i < carts.Length; i++)
            {
                temp[i] = carts[i];
            }
            carts = temp;
            carts[carts.Length - 1] = cart;
        }


    }
}
