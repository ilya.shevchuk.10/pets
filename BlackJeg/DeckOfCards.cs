﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlackJeg
{
    public class DessckOfCart
    {
        private Cart[] carts;
        public DessckOfCart()
        {
            carts = new Cart[36];
            carts[0] = new Cart(6, "6", Suit.Clubs);
            carts[1] = new Cart(6, "6", Suit.Diamonds);
            carts[2] = new Cart(6, "6", Suit.Hearts);
            carts[3] = new Cart(6, "6", Suit.Spades);
            carts[4] = new Cart(7, "7", Suit.Clubs);
            carts[5] = new Cart(7, "7", Suit.Diamonds);
            carts[6] = new Cart(7, "7", Suit.Hearts);
            carts[7] = new Cart(7, "7", Suit.Spades);
            carts[8] = new Cart(8, "8", Suit.Clubs);
            carts[9] = new Cart(8, "8", Suit.Diamonds);
            carts[10] = new Cart(8, "8", Suit.Hearts);
            carts[11] = new Cart(8, "8", Suit.Spades);
            carts[12] = new Cart(9, "9", Suit.Clubs);
            carts[13] = new Cart(9, "9", Suit.Diamonds);
            carts[14] = new Cart(9, "9", Suit.Hearts);
            carts[15] = new Cart(9, "9", Suit.Spades);
            carts[16] = new Cart(10, "10", Suit.Clubs);
            carts[17] = new Cart(10, "10", Suit.Diamonds);
            carts[18] = new Cart(10, "10", Suit.Hearts);
            carts[19] = new Cart(10, "10", Suit.Spades);
            carts[20] = new Cart(2, "Валет", Suit.Clubs);
            carts[21] = new Cart(2, "Валет", Suit.Clubs);
            carts[22] = new Cart(2, "Валет", Suit.Clubs);
            carts[23] = new Cart(2, "Валет", Suit.Clubs);
            carts[24] = new Cart(3, "Дама", Suit.Clubs);
            carts[25] = new Cart(3, "Дама", Suit.Clubs);
            carts[26] = new Cart(3, "Дама", Suit.Clubs);
            carts[27] = new Cart(3, "Дама", Suit.Clubs);
            carts[28] = new Cart(4, "Король", Suit.Clubs);
            carts[29] = new Cart(4, "Король", Suit.Clubs);
            carts[30] = new Cart(4, "Король", Suit.Clubs);
            carts[31] = new Cart(4, "Король", Suit.Clubs);
            carts[32] = new Cart(11, "Туз", Suit.Clubs);
            carts[33] = new Cart(11, "Туз", Suit.Clubs);
            carts[34] = new Cart(11, "Туз", Suit.Clubs);
            carts[35] = new Cart(11, "Туз", Suit.Clubs);
            Mix();

        }
        public Cart GetCart(Cart cart)
        {
            Cart[] temp = new Cart[carts.Length + 1];
            for (int i = 0; i < carts.Length; i++)
            {
                temp[i] = carts[i];
            }
            carts = temp;
            carts[carts.Length - 1] = cart;
            return cart;

        }
        public Cart GetFirstCart()
        {
            Cart[] temp = new Cart[carts.Length - 1];
            Cart resoult = carts[carts.Length - 1];
            for (int i = 0; i < temp.Length; i++)
            {

                temp[i] = carts[i];  
            }
            carts = temp;
            return resoult;
        }
        public void Mix()
        {
            Random random = new Random();
            for (int i = 0; i < 36; i++)
            {
                int a = random.Next(0, 35);
                int b = random.Next(0, 35);
                Cart temp = carts[a];
                carts[a] = carts[b];
                carts[b] = temp;
            }

        }
    }
}
