﻿using System;

namespace LES10
{
    class Program
    {
        static void Main(string[] args)
        {
            string stroka = "BAGGTTCC";
            MakeComplement(stroka);
        }

        public static string Solution(string a, string b)
        {
            if (a.Length > b.Length)
            {
                return b + a + b;
            }
                return a + b + a;
        }
        public static string MakeComplement(string dna)
        {
            string rez="";
            foreach (var item in dna)
            {
                if (item == 'A')
                {
                    rez += "T";
                }
                else if (item == 'T')
                {
                    rez += "A";
                }
                else if (item == 'C')
                {
                    rez += "G";
                }
                else if (item == 'G')
                {
                    rez += "C";
                }
                else
                {
                    rez += item.ToString();
                }


            }
            return rez;
        }

        //static int Select(int op, int a, int b)
        //{
        //    switch (op)
        //    {
        //        case 1: return a + b;
        //        case 2: return a - b;
        //        case 3: return a * b;
        //        default: throw new ArgumentException("Недопустимый код операции");
        //    }
        //}
        public static string MedCase(string dna)
        {
            string rez = "";
            foreach (var item in dna)
            {
                switch (item)
                {
                    case 'A':
                        rez += "T";
                        break;
                    case 'T':
                        rez += "A";
                        break;
                    case 'C':
                        rez += "G";
                        break;
                    case 'G':
                        rez += "C";
                        break;
                    default:
                        rez += item.ToString();
                        break;
                }

            }
            return rez; 
        }
    }


}
