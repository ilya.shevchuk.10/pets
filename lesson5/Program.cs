﻿using System;

namespace lesson5
{
    class Program
    {
        static void Main(string[] args)
        {   

            int[] arr = new int[] { 1, -2, 4, -5 };
            Fun2(arr);

            Fun4(arr);
        }
        public static bool Fun(int[] args)
        {
            int vid = 0;
            int dod = 0;
            for (int i = 0; i <args.Length; i++)
            {
                if (args[i] < 0)
                {
                    dod++;
                }
                else {
                    vid++;
                }
                
            
            }
            if (dod > vid)
            {
                return true;
            }
            if (dod < vid)
            {
                return false;
            }
            return false;
        }

        public static void Fun2(int[] args)
        {
            foreach (var item in args)
            {
                if(item>0)
                {
                    Console.WriteLine(item);   
                }
            }       
        }
        public static void Fun3()
        {
            //0  1  2
            int[,] mas = { { 1, 2, 3 },//0 
                           { 4, 5, 6 },//1
                           { 7, 8, 9 },//2
                           { 10, 11, 12 } };//3

            int rows = mas.GetUpperBound(0) + 1;
            int columns = mas.Length / rows;
            // или так
            // int columns = mas.GetUpperBound(1) + 1;

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    int t = mas[i, j];
                    Console.Write($"{mas[i, j]} \t");
                }
                Console.WriteLine();
            }
        }
        public static void Fun4(int[] nums)
        {
            int temp;
            for (int i = 0; i < nums.Length - 1; i++)
            {
                for (int j = i + 1; j < nums.Length; j++)
                {
                    if (nums[i] > nums[j])
                    {
                        temp = nums[i];
                        nums[i] = nums[j];
                        nums[j] = temp;
                    }
                }
            }
        }
    }
}
