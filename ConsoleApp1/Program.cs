﻿using System;

namespace ConsoleApp1
{

    class Person
    {
        public int age;
        public string name;
        public Person()
        {
        }
        public Person(int age)
        {
            name = "Vasy";
            this.age=age;
        }
       
        public void GetInfo()
        {
            Console.WriteLine($"Vozrast {age} Imya {name}");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Person Bob = new Person();
            Person Vasy = new Person(4);
            Person ilya = new Person {age=23, name="Ilya" };
            Bob.GetInfo();
            Vasy.GetInfo();
            ilya.GetInfo();
            Console.ReadKey();
        }
    }
}
