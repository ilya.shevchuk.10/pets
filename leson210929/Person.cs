﻿using System;
using System.Collections.Generic;
using System.Text;

namespace leson210929
{
    public class Person
    {
        public int age;
        public string name;
        public Sex sex;
        public Person [] childrens;
        public Person(int age, string name, Sex sex, Person [] childrens)
        {
            this.age = age;
            this.name = name;
            this.sex = sex;
            this.childrens = childrens;
        }
        public void PrintInfo()
        {
            Console.WriteLine($"{age} {name} {sex}");
            SortСhildren();
            foreach (var children in childrens)
            {
                Console.WriteLine($"{children.age} {children.name} {children.sex} ");
            }
        }
        private void SortСhildren()
        {
            for (int i = 0; i < childrens.Length-1; i++)
            {
                for (int j = 1; j < childrens.Length; j++)
                {
                    Person temp;
                    if (childrens[i].age > childrens[j].age)
                    {
                        temp = childrens[j];
                        childrens[j] = childrens[i];
                        childrens[i] = temp;
                    }
                }
            }
        }
        public void PirntAge()
        {
            foreach (var children in childrens)
            {
                Console.WriteLine($"{children.age}");
            }
            for (int i = 0; i < childrens.Length; i++)
            {
                Person j = childrens[i];
                Console.WriteLine($"{childrens[i].age}");
            }
        }
        
    }
    public enum Sex
    {
        Men,
        Woman
    }
}
