﻿using System;

namespace leson210929
{
    class Program
    {
        static void Main(string[] args)
        {
            Math math = new Math();
            Console.WriteLine($"{math.Div(10, 2)}");
            Console.WriteLine($"{math.Multiply(3, 5)}");
            Console.WriteLine($"{math.Extent(3, 3)}");
            Console.WriteLine($"{math.Sum(4, 4)}");

            Person p1 = new Person(22, "Andr",Sex.Woman,new Person[0]);
            Person p2 = new Person(21, "Vova", Sex.Woman, null);
            Person p3 = new Person(11, "Vova", Sex.Woman, null);
            Person[] temp = new Person[] {p1,p2,p3 };
            Person father = new Person(45,"Jora",Sex.Men,temp);
            father.PrintInfo();


        }

    }
}
