﻿using System;

namespace CursWork1
{
    class Program
    {
        public static string[] Names = { "илья1", "илья11", "коля", "вова" };
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            bool exit = true;
            while (true)
            {
                Console.WriteLine("Вивід масиву");
                Console.WriteLine("Додавання нового елементу");
                Console.WriteLine("Видалення елементу");
                Console.WriteLine("Сортування масиву");
                Console.WriteLine("Вихід");
                char ch = char.Parse(Console.ReadLine());
                switch (ch)
                {
                    case '1':
                        PrintArr();
                        break;
                    case '2':
                        AddNew();
                        break;
                    case '3':
                        DellArr();
                        break;
                    case '4':
                        SortArr();
                        break;
                    case '0':
                        exit = false;
                        break;
                    default:
                        Console.WriteLine("Ви ввели невірні данні");
                        break;
                }
                Console.ReadKey();
                Console.Clear();

            }

        }
        public static void PrintArr()
        {
            foreach (var item in Names)
            {
                Console.WriteLine(item);
            }
        }

        public static void AddNew()
        {

            string newelem = Console.ReadLine();
            string[] newArr = new string[Names.Length + 1];
            for (int i = 0; i < Names.Length; i++)
            {
                newArr[i] = Names[i];
            }
            newArr = Names;
            Names[Names.Length - 1] = newelem;
            PrintArr();

        }
        public static void DellArr()
        {
            int j = 0;
            int dellEll = Int32.Parse(Console.ReadLine());
            string[] newArr = new string[Names.Length - 1];
            for (int i = 0; i < Names.Length; i++)
            {
                if (dellEll == i)
                {

                    continue;
                }
                newArr[j] = Names[i];

                j++;
            }
            Names = newArr;
            PrintArr();
        }

        public static void SortArr()
        {
            Console.WriteLine("Vu vuihlu v sortyvanya masuvy");
            for (int i = 0; i < Names.Length - 1; i++)
            {
                for (int j = 1; j < Names.Length; j++)
                {
                    if (CompareTo(Names[i], Names[j]) < 0)
                    {
                        string temp = Names[j];
                        Names[j] = Names[i];
                        Names[i] = temp;
                    }
                }
            }
        }
        public static int CompareTo(string str1, string str2)
        {
            int x = -1;
            if (str1.Length < str2.Length)
            {
                x = str1.Length;
            }
            if (str1.Length > str2.Length)
            {
                x = str2.Length;
            }
            if (str1.Length == str2.Length)
            {
                x = str2.Length;
            }
            for (int i = 0; i < x; i++)
            {
                if (str1[i]>str2[i])
                {
                    return -1;
                }
                if (str1[i] < str2[i])
                    return 1;

            }
            if (str1.Length>str2.Length) 
            {
                return -1;
            }
            if (str1.Length < str2.Length)
            {
                return 1;
            }
            return 0;
        }
    }

}

