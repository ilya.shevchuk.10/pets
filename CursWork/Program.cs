﻿using System;

namespace CursWork
{
    class Program
    {
        public static string[] Names = { "Ilya", "Andrii", "Kolya", "Vova" };
        static void Main(string[] args)
        {

            //string a = Console.ReadLine();


            Console.WriteLine("Hello World!");
            bool exit = true;
            while (exit)
            {
                Console.WriteLine("Виберіть пункт меню: ");
                Console.WriteLine("1.Відображення всіх елементів масиву: ");
                Console.WriteLine("2.Додавання нового елементу масиву: ");
                Console.WriteLine("3.Видалення елементу з масива: ");
                Console.WriteLine("4.Сортування масиву  ");
                Console.WriteLine("0.Вихід: ");
                var i = Console.ReadKey();
                Console.WriteLine();
                switch (i.KeyChar)
                {
                    case '1':
                        PunktMenu1();
                        break;
                    case '2':
                        PunktMenu2();
                        break;
                    case '3':
                        PunktMenu3();
                        break;
                    case '4':
                        PunktMenu4();
                        break;
                    case '0':
                        exit = false;
                        break;
                    default:
                        Console.WriteLine("Nekorktni dani");
                        break;
                }
                Console.ReadKey();
                Console.Clear();
            }
        }
        public static void PunktMenu1()
        {
            Console.WriteLine("Vu vubralu punkt 1");
            foreach (var item in Names)
            {
                Console.WriteLine(item);
            }

        }
        public static void PunktMenu2()

        {
            Console.WriteLine("Vu vubralu punkt 2");
            var input = Console.ReadLine();
            string[] temp = new string[Names.Length + 1];
            for (int i = 0; i < Names.Length; i++)
            {
                temp[i] = Names[i];//Присвоєння значеннь 
            }
            Names = temp;//Присвоє нове значення масиву
            Names[Names.Length - 1] = input;
            PunktMenu1();

        }
        public static void PunktMenu3()
        {
            
            int index = 1;
            string[] temp = new string[Names.Length - 1];
            int j = 0;
            for (int i = 0; i < Names.Length; i++)
            {
                if (index == i)
                {
                    continue;
                }
                temp[j] = Names[i];
                j++;
            }
            Names = temp;
            PunktMenu1();
            Console.WriteLine("Vu vubralu punkt 3");
        }
        public static void PunktMenu4()
        {
            for (int i = 0; i < Names.Length; i++)
            {
                for (int j = 0; j < Names.Length-1; j++)
                {
                    if (Names[j].CompareTo(Names[j + 1]) > 0)
                    {
                        string temp = Names[j];
                        Names[j] = Names[i];
                        Names[i] = temp;
                    }
                }
            }
            PunktMenu1();
            Console.WriteLine("Vu vubralu punkt 4");
        }

        
    }
}
